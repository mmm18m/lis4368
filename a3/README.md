# LIS4368

## Mary Meberg

### Assignment 3 Requirements:


1. Install MySQL
2. Create an ERD in MySQL
3. Create Data Inserts
4. Forward Engineer and Provide Files
5. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of mobile recipe app
* Provide Skillsets

#### ERD Screenshots:
![ERDModel](img/model.PNG)

#### localhost Screenshots:
![ERDModel](img/site.PNG)

#### DOCS
[a3.mwb](https://bitbucket.org/mmm18m/lis4368/src/master/a3/docs/a3.mwb)

[a3.sql](https://bitbucket.org/mmm18m/lis4368/src/master/a3/docs/a3.sql)

