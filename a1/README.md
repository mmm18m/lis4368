# LIS4368 - Adv. Web Application Development

## Mary Meberg

### **Assignment 1 Requirements:**

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)


#### **README.md file should include the following items:**

* Screenshot of running java Hello
* Screemshot of running http://localhost:9999/
* Bitbucket tutorial links
* git commands with short description

#### Git commands w/short descriptions:

1. git init: Creates a new Git repository.
2. git status: Displays the state of the working directory and the staging area.
3. git add: Adds a change in the working directory to the staging area
4. git commit: Saves your changes to the local repository.
5. git push: Uploads local repository content to a remote repository.
6. git pull: Fetches and downloads content from a remote repository updates the local repository to match.
7. git branch: Lists all the branches in your repo, and tells you what branch you're currently in.

#### Assignment Screenshots:




| *Screenshot of running java Hello*: | *Screenshot Tomcat*: | *Image of site*: |
|-------------------------------------|------------------------------------------------|------------------------------------------------|
| ![JDK Installation Screenshot](img/jdk_installation.png) | ![Android Studio Installation Screenshot](img/tomcat_install.PNG)| ![Android Studio Installation Screenshot](img/wheel.PNG)|

A1 Page:
![JDK Installation Screenshot](img/a1.PNG)

    


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mmm18m/stationlocation/src/master/ "Bitbucket Station Locations")
