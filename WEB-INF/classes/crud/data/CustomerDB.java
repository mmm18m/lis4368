package crud.data;

import java.sql.*;
import java.util.ArrayList;

import crud.business.Customer;
import crud.data.DBUtil;
import java.sql.*;
import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class CustomerDB {

    
    public static int insert(Customer customer){

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        String query = 
        "INSERT INTO customer (cust_fname, cust_lname, cust_street, cust_city, cust_state, cust_zip, cust_phone, cust_email, cust_balance, cust_total_sales, cust_notes) " + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";

        try {
            ps = connection.prepareStatement(query);
        
            ps.setString(1, customer.getFname());
            ps.setString(2, customer.getLname());
            ps.setString(3, customer.getStreet());
            ps.setString(4, customer.getCity());
            ps.setString(5, customer.getState());
            ps.setInt(6, customer.getZip());
            ps.setLong(7, customer.getPhone());
            ps.setString(8, customer.getEmail());
            ps.setBigDecimal(9, customer.getBalance());
            ps.setBigDecimal(10, customer.getTotalSales());
            ps.setString(11, customer.getNotes());

            return ps.executeUpdate();
        } catch (SQLException e){
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
 



    }

    public static Customer selectCustomer(String id){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT * FROM customer WHERE cust_id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            Customer customer = null;
            if (rs.next()){
                customer = new Customer();
                customer.setId(id);
                customer.setFname(rs.getString("cust_fname"));
                customer.setLname(rs.getString("cust_lname"));
                customer.setStreet(rs.getString("cust_street"));
                customer.setCity(rs.getString("cust_city"));
                customer.setState(rs.getString("cust_state"));
                customer.setZip(rs.getInt("cust_zip"));
                customer.setPhone(rs.getLong("cust_phone"));
                customer.setEmail(rs.getString("cust_email"));
                customer.setBalance(rs.getBigDecimal("cust_balance"));
                customer.setTotalSales(rs.getBigDecimal("cust_total_sales"));
                customer.setNotes(rs.getString("cust_notes"));
            }//end if
            return customer;
            } catch (SQLException e){
                System.out.println(e);
                return null;
            } finally {
                DBUtil.closePreparedStatement(ps);
                pool.freeConnection(connection);
            }
        }

    public static ArrayList<Customer> selectCustomers(){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT * FROM customer";

        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList<Customer> customers = new ArrayList<Customer>();
            while (rs.next()){
                Customer customer = new Customer();
                customer.setId(rs.getString("cust_id"));
                customer.setFname(rs.getString("cust_fname"));
                customer.setLname(rs.getString("cust_lname"));
                customer.setStreet(rs.getString("cust_street"));
                customer.setCity(rs.getString("cust_city"));
                customer.setState(rs.getString("cust_state"));
                customer.setZip(rs.getInt("cust_zip"));
                customer.setPhone(rs.getLong("cust_phone"));
                customer.setEmail(rs.getString("cust_email"));
                customer.setBalance(rs.getBigDecimal("cust_balance"));
                customer.setTotalSales(rs.getBigDecimal("cust_total_sales"));
                customer.setNotes(rs.getString("cust_fname"));
                customers.add(customer);
            }
            return customers;
        } catch (SQLException e){
            System.out.println(e);
            return null;
            
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }


    }

    public static int update (Customer customer){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null; 
        String query = "UPDATE customer SET "
        + "cust_fname = ?, "
        + "cust_lname = ?, "
        + "cust_street = ?, "
        + "cust_city = ?, "
        + "cust_state = ?, "
        + "cust_zip = ?, "
        + "cust_phone = ?, "
        + "cust_email = ?, "
        + "cust_balance = ?, "
        + "cust_total_sales = ?, "
        + "cust_notes = ? "
        + "WHERE cust_id = ?";

        try {
            ps = connection.prepareStatement(query);

            ps.setString(1, customer.getFname());
            ps.setString(2, customer.getLname());
            ps.setString(3, customer.getStreet());
            ps.setString(4, customer.getCity());
            ps.setString(5, customer.getState());
            ps.setInt(6, customer.getZip());
            ps.setLong(7, customer.getPhone());
            ps.setString(8, customer.getEmail());
            ps.setBigDecimal(9, customer.getBalance());
            ps.setBigDecimal(10, customer.getTotalSales());
            ps.setString(11, customer.getNotes());

            return ps.executeUpdate();

        } catch (SQLException e){
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }


    }//end update

    public static int delete (Customer customer){
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null; 

        String query = "DELETE FROM customer WHERE cust_id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, customer.getId());

            return ps.executeUpdate();
        } catch (SQLException e){
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }//end delete

}