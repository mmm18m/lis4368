# LIS4368 - Advanced Web Application Development

## Mary Marguerite Meberg

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a1/README.md)
	* Install JDK
* Install Tomcat
* Provide screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a2/README.md)
	* Complete Servlet tutorials
* Provide screenshots of localhost pages
	* Update course site

3. [A3 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a3/README.md)
    * Complete ERD
* Provide database with inserts
* Provide database files (a3.mwb and a3.sql)
	* Update course site

4. [A4 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a4/README.md)
	* Chapter Questions
* Provide screenshots of localhost pages
	* Skillsets

  
5. [A5 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/a5/README.md)
	* Chapter Questions
* Provide screenshots of localhost pages
	* Skillsets

 
6. [P1 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/p1/README.md)
	* Chapter Questions
* Provide screenshots of localhost pages
	* Update course site


7. [P2 README.md](https://bitbucket.org/mmm18m/lis4368/src/master/p2/README.md)
	* Chapter Questions
* Provide screenshots of localhost pages
	* Update course site
