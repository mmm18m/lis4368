# LIS4368

## Mary Meberg

### Assignment 5 Requirements:


1. Add functionality to A4
2. Skillset
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of validation
* Provide Skillsets

#### Pre-Submission Screenshot:
![ERDModel](img/pre-entry.PNG)

#### Thank You Screenshot:
![ERDModel](img/validationPassed.PNG)

#### Database Screenshot:
![ERDModel](img/sql_entry.PNG)

#### Skillsets:
| Skillset 13: | Skillset 14: | Skillset 15: |
|--------------|--------------|--------------|
|![ERDModel](img/ss13.PNG)|![ERDModel](img/ss14.PNG)|![ERDModel](img/ss15.PNG)|





