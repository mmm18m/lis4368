import java.util.InputMismatchException;
import java.util.Scanner;

public class LIS4368SS16 {

    public static void printDirections(){
        System.out.println("Program evaluates largest of three integers");
        System.out.println("Note: Program checks for integer and non-numeric values");
    }

    public static void main (String[] args){

        Scanner scannyboi = new Scanner(System.in);
        int intA = 0, intB = 0, intC = 0;

        printDirections();
        for (int i = 0; i <= 2; i++){
            switch (i) {
                    case 0: 
                        System.out.println("Please enter first number");
                        try {
                            intA = scannyboi.nextInt();
                        } catch (InputMismatchException e) {
                            System.out.println("Please enter a valid integer");
                            scannyboi.next();
                            intA = scannyboi.nextInt();
                        }
                        break;
                case 1:
                    System.out.println("Please enter second number");
                    try {
                        intB = scannyboi.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.println("Please enter a valid integer");
                        scannyboi.next();
                        intB = scannyboi.nextInt();
                    }
                    break;
                case 2:
                System.out.println("Please enter third number");
                    try {
                        intC = scannyboi.nextInt();
                    } catch (InputMismatchException e) {
                        System.out.println("Please enter a valid integer");
                        scannyboi.next();
                        intC = scannyboi.nextInt();
                    }
                    break;
            }//end switch
        }//end for



    if (intA > intB){
        if  (intA > intC){
            System.out.println("The first number, " + intA + ", is the largest number");
        } else {
            System.out.println("The third number, " + intC + ", is the largest number");
        }
    } else {
        if (intB > intC){
            System.out.println("The second number, " + intB + ", is the largest number");
        } else {
            System.out.println("The third number, " + intC + ", is the largest number");
        }
    }



    }//end main
}//end class