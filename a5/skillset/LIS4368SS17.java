import java.util.InputMismatchException;
import java.util.Scanner;


public class LIS4368SS17 {

    public static void printDirections(){
        System.out.println("Program uses nonvalue-returning methods to add, subtract, multiply, divide and power floating point numbers, rounded to two decimal places");
        System.out.println("Note: Program checks for non-numeric values and division by 0");
        System.out.println("\nMathematical operations: A = Add, S = Subtract, M = Multiply, D = Divide, P = Power");
    }

    public static void divide(){
        Scanner scanner = new Scanner(System.in);
        double one = 0; 
        double two = 0;

        System.out.println("Enter number 1:");
        try {
            one = scanner.nextDouble();
        } catch (InputMismatchException e){
            System.out.println("Invalid value. Enter number value");
            scanner.next();
            one = scanner.nextDouble();
        }

        System.out.println("Enter number 2:");
        two = scanner.nextDouble();
        while (two == 0){
            System.out.println("Dividing by 0 is not possible. Re-enter a value");
            two = scanner.nextDouble();
        }
        double result = (one / two);

        System.out.println(one + " divided by "  + two + " is equal to " + result);

    }

    public static void power(){
        Scanner scanner = new Scanner(System.in);
        double one = 0; 
        double two = 0;

        System.out.println("Enter number 1:");
        try {
            one = scanner.nextDouble();
        } catch (InputMismatchException e){
            System.out.println("Enter number value");
            scanner.next();
            one = scanner.nextDouble();
        }

        System.out.println("Enter number 2:");
        two = scanner.nextDouble();

        double result = Math.pow(one, two);
        System.out.println(one + " to the power of " + two + " is equal to " + result + "0");

    }

    public static void main (String[] args){

        Scanner scannyboi = new Scanner(System.in);
        char selection = ' ';
        char selectionUpper = ' ';
        boolean validSelection = true;

        printDirections();

        try {
            selection = scannyboi.next().charAt(0);
            selectionUpper = Character.toUpperCase(selection);
        } catch (InputMismatchException e){
            System.out.print("Please enter a letter value");
            selection = scannyboi.next().charAt(0);
            selectionUpper = Character.toUpperCase(selection);
        }

        if (selectionUpper != 'A' && selectionUpper != 'S' && selectionUpper != 'M' && selectionUpper != 'D' && selectionUpper != 'P'){
            validSelection = false;
            while (validSelection == false){
                System.out.println("You did not enter a valid letter value. Please re-enter:");
                selection = scannyboi.next().charAt(0);
                selectionUpper = Character.toUpperCase(selection);

                if (selectionUpper == 'A' || selectionUpper == 'S' || selectionUpper == 'M' || selectionUpper == 'D' || selectionUpper == 'P') {
                    validSelection = true;
                }
            }
        }


        switch (selectionUpper){
            case 'A':
                System.out.println("You have selected add:");
                break;
            case 'S':
                System.out.println("You have selected subtact:");
                break;
            case 'M':
                System.out.println("You have selected multiply:");
                break;
            case 'D':
                System.out.println("You have selected divide:");
                divide();
                break;
            case 'P':
                System.out.println("You have selected power:");
                power();
                break;
        }

    }//end main
}//end class