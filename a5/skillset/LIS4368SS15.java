import java.util.InputMismatchException;
import java.util.Scanner;

public class LIS4368SS15 {

    public static void printDirections(){
        System.out.println("Program swaps two integers");
        System.out.println("Note: Program checks for integer and non-numeric values");
    }

    public static int getInputA(){
        int intA = 0;
        Scanner scannyboi = new Scanner(System.in);
        
        System.out.println("Please enter first number");
        try {
            intA = scannyboi.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Please enter a valid integer");
            scannyboi.next();
            intA = scannyboi.nextInt();
        }//end catch

        return intA;

    }//end getInput

    public static int getInputB() {
        int intB = 0;
        Scanner scannyboi = new Scanner(System.in);

        System.out.println("Please enter second number");
        try {
            intB = scannyboi.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Please enter a valid integer");
            scannyboi.next();
            intB = scannyboi.nextInt();
        }//end catch

        return intB;
    }

    public static void swapInt(){
        int intABefore = getInputA();
        int intBBefore = getInputB();
        int intBAfter = intABefore;
        int intAAfter = intBBefore;

        System.out.println("");
        System.out.println("/////Before Swapping/////");
        System.out.println("First number is: " + intABefore);
        System.out.println("Second number is: " + intBBefore);
        System.out.println("");
        System.out.println("/////After Swapping/////");
        System.out.println("First number is: " + intAAfter);
        System.out.println("Second number is: " + intBAfter);

    }


    public static void main(String[] args){

        printDirections();
        swapInt();



    }//end main
}//end class