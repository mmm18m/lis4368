# LIS4368

## Mary Meberg

### Project 2 Requirements:


1. Research Data Validation
2. Create Form with Database Attributes
3. Create Data Validation System On Form
4. Chapter Questions

#### Customer Entry:
| Customer Entry: | Thanks.jsp: | 
|--------------|--------------|
|![ERDModel](img/addCustom.PNG)|![ERDModel](img/thanks.PNG)|


#### Edit Form:
| Table With New Customer Entry: | Edit Entry: | 
|--------------|--------------|
|![ERDModel](img/table.PNG)|![ERDModel](img/editEntry.PNG)|


#### Edit Form + Delete :
| Table With New Edits: | Table With Delete Warning| Table With After Deletion| 
|--------------|--------------|--------------|
|![ERDModel](img/entryEDITED.PNG)|![ERDModel](img/deleteEntry.PNG)|![ERDModel](img/afterDELETE.PNG)|

New Entry + Edit On Bottom of Screen 


#### SQL Entries:
| Table With New Entry: | Table With Edits| Table With After Deletion| 
|--------------|--------------|--------------|
|![ERDModel](img/sqlNew.PNG)|![ERDModel](img/sqlEdit.PNG)|![ERDModel](img/sqlEntries.PNG)|



