# LIS4368 - Adv. Web Application Development

## Mary Meberg

### Assignment 2 Requirements:

1. Complete Servlet Tutorial
2. Chapter Questions (Chs 3,4)
3. Provide photos/links to localhost sites


#### Localhost:

|  [Directory](http://localhost:9999/hello/): | [helloworld.html](http://localhost:9999/hello/): | [Say Hello](http://localhost:9999/hello/sayhello): |
|-----------------|----------------|---------------|
|![Skillshot 1](img/directory.PNG)|![Skillshot 2](img/hello_html.PNG)|![Skillshot 3](img/sayhello.PNG)|


|  [HiServlet](http://localhost:9999/hello/sayhi):|  [Bookshop](http://localhost:9999/hello/querybook.html): | [Query](http://localhost:9999/hello/query?author=Kumar): | 
|-----------------|----------------|----------------|
|![Skillshot 1](img/sayhi.PNG)|![Skillshot 1](img/bookshop.PNG)|![Skillshot 2](img/query.PNG)|

#### Site

![placeholder](img/site.PNG)