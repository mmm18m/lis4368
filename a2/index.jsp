<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 18:35:01 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - Assignment 2</title>

	<%@ include file="/css/include_css.jsp" %>	
	
	<style>

.navbar-inverse
{
	background-image: linear-gradient(to bottom,#7AD7F0 0,#DBF3FA 100%);
	border-color: #92DFF3;

}

.navbar-inverse .navbar-nav>li>a {
	color:#6694BE;
}
.navbar-inverse .navbar-brand {
	color:#6694BE;
}
h1
{
	text-align: center;
}
	</style>
	
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<h4>Local Host Images:</h4>
					<img src="img/directory.PNG" class="img-responsive center-block" alt="JDK Installation" />

					<br /> <br />
					<img src="img/hello_html.PNG" class="img-responsive center-block" alt="Tomcat Installation" />
					
					<br /> <br />
					<img src="img/sayhello.PNG" class="img-responsive center-block" alt="Tomcat Installation" />
					<br /> <br />
					<img src="img/sayhi.PNG" class="img-responsive center-block" alt="Tomcat Installation" />

					<h4>Bookstore:</h4>
					<br /> <br />
					<img src="img/bookshop.PNG" class="img-responsive center-block" alt="Tomcat Installation" />

					<br /> <br />
					<img src="img/query.PNG" class="img-responsive center-block" alt="Tomcat Installation" />


	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		

</body>
</html>
