import java.util.Scanner;
import java.util.Scanner;

public class ReadNumWords
{

    static int wordcount(String string)  
    {  
      int count=0;  
  
        char ch[]= new char[string.length()];     
        for(int i=0;i<string.length();i++)  
        {  
            ch[i]= string.charAt(i);  
            if( ((i>0)&&(ch[i]!=' ')&&(ch[i-1]==' ')) || ((ch[0]!=' ')&&(i==0)) )  
                count++;  
        }  
        return count;  
    }  

    public static void main (String[] args)
    {   Scanner scnr = new Scanner(System.in);

       System.out.println("Program captures user input, writes to and reads from same file, and counts number of words");

       System.out.println("\nPlease enter text: ");
       String text = scnr.nextLine();

       System.out.println("Saved to file \"filecountwords.txt\"");
       System.out.println("Number of words: " + wordcount(text));









        
    }
}