import java.util.Scanner;

public class CountCharacter
{

    public static void main(String[] args)
    {
        System.out.println("Program counts number and types of characters: that is, letters, spaces, numbers and other characters.\n");
        System.out.println("Hint: You may find the following methods helpful: isLetter(), is Digit(), is SpaceChar().\n");
        System.out.println("Additionally, you could add the functionality of testing for upper vs lower case letters.\n");

        String test;
        Scanner scnr = new Scanner(System.in);
        int letter = 0;
        int spaces = 0;
        int numbers = 0;
        int other = 0; 

        System.out.print("Please enter string: ");
        test = scnr.nextLine();
        System.out.println();

        for (int i = 0; i < test.length(); i++)
        {
            if (Character.isLetter(test.charAt(i)))
            {
                letter ++;
                
            }

            else if (Character.isSpaceChar(test.charAt(i)))
            {
                spaces ++;
                
            }

            else if (Character.isDigit(test.charAt(i)))
            {
                numbers ++;
                
            }

            else
            {
                other ++;
                
            }


        }
        
        System.out.println("Your string \"" + test + "\" has the following number and types of characters: ");
        
        System.out.println("letter(s): " + letter);
        System.out.println("space(s): " + spaces);
        System.out.println("number(s) " + numbers);
        System.out.println("other character(s) " + other);
    
    }
}