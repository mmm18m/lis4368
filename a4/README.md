# LIS4368

## Mary Meberg

### Assignment 4 Requirements:


1. Get server-side validation working
2. Skillset
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of validation
* Provide Skillsets

#### Failed Validation Screenshot:
![ERDModel](img/validationFailed.PNG)


#### Passed Validation Screenshot:
![ERDModel](img/validationPassed.PNG)

#### Skillsets:
| Skillset 10: | Skillset 11: | Skillset 12: |
|--------------|--------------|--------------|
|![ERDModel](img/ss10.PNG)|![ERDModel](img/ss11.PNG)|![ERDModel](img/ss12.PNG)|





