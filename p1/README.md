# LIS4368

## Mary Meberg

### Project 1 Requirements:


1. Research Data Validation
2. Create Form with Database Attributes
3. Create Data Validation System On Form
4. Chapter Questions

#### Homescreen Screenshots:
![ERDModel](img/home.PNG)

#### Valid Data Screenshots:
![ERDModel](img/yesValid.PNG)

#### Invalid Data Screenshots:
![ERDModel](img/noValid.PNG)



