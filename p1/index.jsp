<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 18:35:01 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - Project 1</title>

	<%@ include file="/css/include_css.jsp" %>	
	
	<style>

.navbar-inverse
{
	background-image: linear-gradient(to bottom,#7AD7F0 0,#DBF3FA 100%);
	border-color: #92DFF3;

}

.navbar-inverse .navbar-nav>li>a {
	color:#6694BE;
}
.navbar-inverse .navbar-brand {
	color:#6694BE;
}
h1
{
	text-align: center;
}
h2
{
	text-align: center;
}

.form-group
{
    margin-left:auto;margin-right:auto;text-align:center
}
	</style>
	
	
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					
			<div class="page-header">
				<%@ include file="global/header.jsp" %>
			</div>

					<h2>Pet Stores</h2>

						<form id="petShopForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">Name:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="name" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Street:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="street" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">City:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="city" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">State:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="2" name="state" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Zip:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="9" name="zip" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Phone:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="10" name="phone" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Email:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="100" name="email" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">URL:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="100" name="url" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">YTD Sales:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="11"name="ytd" />
										</div>
								</div>

								
								<div class="form-group">
									<label class="col-sm-4 control-label">Notes:</label>
									<div class="col-sm-4">
											<input type="text" class="form-control" maxlength="255"name="notes" />
									</div>
							</div>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" style="margin-left:auto;margin-right:auto;text-align:center" name="add" value="Add">Add</button>
										</div>
								</div>
						</form>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

 <%@ include file="/js/include_js.jsp" %>		

 <script type="text/javascript">
	//See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
	$(document).ready(function() {
   
	   $('#petShopForm').formValidation({
			   message: 'This value is not valid',
			   icon: {
					   valid: 'fa fa-check',
					   invalid: 'fa fa-times',
					   validating: 'fa fa-refresh'
			   },
			   fields: {
					   name: {
							   validators: {
									   notEmpty: {
										message: 'Name required'
									   },
									   stringLength: {
											   min: 1,
											   max: 30,
										message: 'Name no more than 30 characters'
									   },
									   regexp: {
										   //alphanumeric, hyphens, underscores, and spaces
										   //regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										   //similar to: (though, \w supports other Unicode characters)
											   regexp: /^[\w\-\s]+$/,
										   message: 'Name can only contain letters, numbers, hyphens, and underscore'
									   },									
							   },
					   },
   
					   street: {
							   validators: {
									   notEmpty: {
											   message: 'Street required'
									   },
									   stringLength: {
											   min: 1,
											   max: 30,
											   message: 'Street no more than 30 characters'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									   message: 'Street can only contain letters, numbers, commas, hyphens, or periods'
									   },									
							   },
					   },
   
					   city: {
							   validators: {
									   notEmpty: {
											   message: 'City required'
									   },
									   stringLength: {
											   min: 1,
											   max: 30,
											   message: 'City no more than 30 characters'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									   message: 'City can only contain letters, numbers, commas, hyphens, or periods'
									   },									
							   },
					   },
   
					   state: {
							   validators: {
									   notEmpty: {
											   message: 'State required'
									   },
									   stringLength: {
											   min: 2,
											   max: 2,
											   message: 'State must be 2 characters'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^[a-zA-Z]+$/,		
									   message: 'State can only contain letters'
									   },									
							   },
					   },
   
					   zip: {
							   validators: {
									   notEmpty: {
											   message: 'Zip required'
									   },
									   stringLength: {
											   min: 5,
											   max: 9,
											   message: 'Zip must be between 5 and 9 digits'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^[0-9]+$/,		
									   message: 'Zip can only contain numbers'
									   },									
							   },
					   },
   
					   phone: {
							   validators: {
									   notEmpty: {
											   message: 'Phone required'
									   },
									   stringLength: {
											   min: 10,
											   max: 10,
											   message: 'Phone must be 10 digits'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^[0-9]+$/,		
									   message: 'Phone can only contain numbers'
									   },									
							   },
					   },
   
					   email: {
							   validators: {
									   notEmpty: {
											   message: 'Email required'
									   },
									   stringLength: {
											   min: 1,
											   max: 100,
											   message: 'Email no more than 100 characters'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,		
									   message: 'Please enter a valid email address'
									   },									
							   },
					   },
   
					   url: {
							   validators: {
									   notEmpty: {
											   message: 'URL required'
									   },
									   stringLength: {
											   min: 1,
											   max: 100,
											   message: 'URL no more than 100 characters'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,		
									   message: 'Please enter a valid URL'
									   },									
							   },
					   },
   
					   ytd: {
							   validators: {
									   notEmpty: {
											   message: 'YTD sales required'
									   },
									   stringLength: {
											   min: 1,
											   max: 11,
											   message: 'YTD sales can be no more than 11 characters including decimal'
									   },
									   regexp: {
										   //street: only letters, numbers, commas, hyphens, space character, and periods
										   regexp: /^[0-9\.]+$/,		
									   message: 'YTD sales can only contain numbers and a decimal point'
									   },									
							   },
					   },
					   
			   }
	   });
   });
   </script>

</body>
</html>
