<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mary M. Meberg">
	<link rel="icon" href="favicon.ico">

	<title>My Online Portfolio</title>
	
	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
h1
{
	text-align: center;
}
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;

}

.navbar-inverse
{
	background-image: linear-gradient(to bottom,#7AD7F0 0,#DBF3FA 100%);
	border-color: #92DFF3;

}

.navbar-inverse .navbar-nav>li>a {
	color:#6694BE;
}
.navbar-inverse .navbar-brand {
	color:#6694BE;
}
</style>
	
</head>
<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
		id="myCarousel"
				class="carousel"
				data-interval="1000"
				data-pause="hover"
				data-wrap="true"
				data-keyboard="true"			
				data-ride="carousel">
		
	<!-- Carousel indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>   
		<!-- Carousel items -->
		<div class="carousel-inner">

			<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
			<div class="active item" style="background: url(https://media.istockphoto.com/photos/bright-blue-defocused-blurred-motion-abstract-background-picture-id1047234038?k=6&m=1047234038&s=612x612&w=0&h=O1lP8GIn46sboZL5bnMsznd4A1tRNJ7iXm1MMVh5I5c=) no-repeat left center; background-size: cover;">
				<div class="container">
					<div class="carousel-caption">
						<h3>Welcome</h3>
						<p class="lead"><a href="https://bitbucket.org/mmm18m/lis4381/src/master/" style="color: #fff"> This is a collection of works produced by Mary Meberg for LIS4368. Click to be led to portfolio BitBucket account.</a></p>
					</div>
				</div>
			</div>

				<div class="item">
					<div class="carousel-caption" style="background-color: rgba(228, 161, 60, 0.95);">
						<p style="font-size: 40px;">Where we make art.</p>									
					</div>
					<img src="img/artgallery.jpg" alt="Slide 2" class="contain">
				</div>

				<div class="item" style="background-image: url(img/color.jpg); background-size: cover;">
					<div class="carousel-caption">
						<h3>Contact Me!</h3>
						<p>Email me at mmm18m@my.fsu.edu!</p>	
														
					</div>
					
				</div>

			</div>
			<!-- Carousel nav -->
			<a class="carousel-control left" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="carousel-control right" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
			</div>
		</div>
		<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>
